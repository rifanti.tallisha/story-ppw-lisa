from django.forms import ModelForm
from .models import kegiatan, peserta

class KegiatanForm(ModelForm):
    class Meta:
        model = kegiatan
        fields = '__all__'

class PesertaForm(ModelForm):
    class Meta:
        model = peserta
        fields = '__all__' 