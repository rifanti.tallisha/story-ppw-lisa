from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import kegiatan, peserta
from .views import home, form_kegiatan, form_peserta, halamankegiatan

class TestStory6(TestCase):
    # cek apakah url tersedia
    def test_url(self):
        response = Client().get('/halamankegiatan')
        self.assertEqual(response.status_code,200)

    def test_url_2(self):
        response = Client().get('/formkegiatan')
        self.assertEqual(response.status_code,200)

    # cek views
    def test_views(self):
        found = resolve('/halamankegiatan')
        self.assertEqual(found.func, halamankegiatan)

    def test_views_2(self):
        found = resolve('/formkegiatan')
        self.assertEqual(found.func, form_kegiatan)

    # cek apakah template yang direturn sudah benar apa belum
    def test_template(self):
        response = Client().get('/halamankegiatan')
        self.assertTemplateUsed(response, 'main/PageKegiatan.html')

    def test_template_2(self):
        response = Client().get('/formkegiatan')
        self.assertTemplateUsed(response, 'main/menambahkegiatan.html')

    #cek models
    def test_model_return(self):
        kegiatan.objects.create(tambahkegiatan="ppw")
        result = kegiatan.objects.get(id=1)
        self.assertEqual(str(result), "ppw")

    def test_model_func(self):
        kegiatan.objects.create(tambahkegiatan="ppw")
        cnt = kegiatan.objects.all().count()
        self.assertEqual(cnt, 1)