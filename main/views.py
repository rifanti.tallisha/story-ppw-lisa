from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import KegiatanForm, PesertaForm
from .models import kegiatan, peserta



def home(request):
    return render(request, 'main/home.html')

def form_kegiatan(request):
    tambah = KegiatanForm(request.POST)
    response = {
        "tambah_kegiatan" : KegiatanForm,
    }
    print(tambah.is_valid())
    if(tambah.is_valid() and request.method == 'POST'):
        tambah.save()
        return HttpResponseRedirect('/halamankegiatan')
    return render(request, 'main/menambahkegiatan.html', response)

def halamankegiatan(request):
    tambah = KegiatanForm(request.POST)
    response = {
        "act" : kegiatan.objects.all(),
        "anggotanya" : PesertaForm,
    }
    return render(request, 'main/PageKegiatan.html', response)

def form_peserta(request, id=None):
    tambah = PesertaForm(request.POST)
    kegiatann = kegiatan.objects.get(id=id)
    print(tambah.is_valid())
    if(request.method == 'POST'):
        nama = tambah["pesertanya"].value()
        anggt = peserta(activity=kegiatann, pesertanya=nama)
        anggt.save()
    return HttpResponseRedirect('/halamankegiatan')
