from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('formkegiatan', views.form_kegiatan, name='kegiatan'),
    path('halamankegiatan', views.halamankegiatan, name='utama'),
    path('tambahanggota/<int:id>', views.form_peserta, name='psrt')
]
