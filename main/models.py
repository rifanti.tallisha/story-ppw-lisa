from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class kegiatan(models.Model):
    tambahkegiatan = models.CharField(max_length = 200)

    def __str__(self):
        return "{}".format(self.tambahkegiatan)

class peserta(models.Model):
    activity = models.ForeignKey(kegiatan, on_delete=models.CASCADE, related_name='anggota')
    pesertanya = models.CharField(max_length=100)

    def __str__(self):
        return "{:s}".format(self.pesertanya)

class Post(models.Model):
    author = models.ForeignKey(kegiatan, on_delete = models.CASCADE)
    content = models.CharField(max_length=500)
    published_date = models.DateTimeField(default=timezone.now)
