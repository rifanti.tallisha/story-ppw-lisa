from django.contrib import admin
from .models import peserta, kegiatan

# Register your models here.
admin.site.register(peserta)
admin.site.register(kegiatan)